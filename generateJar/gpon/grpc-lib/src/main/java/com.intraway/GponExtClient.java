package com.intraway;

import com.google.common.annotations.VisibleForTesting;
import gprcextservice.WorkerGrpc;
import gprcextservice.WorkerGrpc.WorkerBlockingStub;
import gprcextservice.WorkerGrpc.WorkerStub;
import io.grpc.Channel;

import gprcextservice.JobRequest;
import gprcextservice.JobResponse;
import com.google.protobuf.Message;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GponExtClient {

    private static final Logger logger = Logger.getLogger(GponExtClient.class.getName());

    private final WorkerBlockingStub blockingStub;
    private final WorkerStub asyncStub;

    public GponExtClient(Channel channel) {
        blockingStub = WorkerGrpc.newBlockingStub(channel);
        asyncStub = WorkerGrpc.newStub(channel);
    }

    public void postClientLookup() {
        try {
            info("*** postClientLookup: lat={0} lon={1}");
            JobRequest request = JobRequest.newBuilder().setCHAddr("F7A8385651C4").build();
            JobResponse response = blockingStub.postClientLookup(request);
            info("*** postClientLookup: lat={0} lon={1}", response);
        } catch (Exception e) {
            warning("RPC failed: {0}", e.getMessage());
            return;
        }

    }
    private void info(String msg, Object... params) {
        logger.log(Level.INFO, msg, params);
    }

    private void warning(String msg, Object... params) {
        logger.log(Level.WARNING, msg, params);
    }
}