# Jmeter plugin para agregar el contrato Grpc (IPv4)

* Es necesario tener los datos cargados en base de datos para realizar las pruebas con N cantidad de devices. Para esto se utiliza **devicefixtures**

* No es necesario levantar la aplicacion de angular para editar la topologia.

* Las pruebas de las extensiones escritas en GO con Jmeter solo requieren Hazelcast, mysql, kafka y la aplicacion del logic ejecutandose.

* Puedes usar delve para hacer debug pero esto realentiza la ejecucion.

* Requiere de una topologia minima, y datos cargados en la base de datos MySql.

* Esta solucion esta basada en un plugin que fue compartido por una empresa vietnamita llamada "Zalopay" que es una plataforma de pagos. De todas las opciones es el mas sencillo que consegui para la epoca, quizas hoy en dia se consigue alguno adicional.

* El repo original esta aca: https://github.com/zalopay-oss/jmeter-grpc-plugin

* Para ver un poco mas de documentacion de grpc y Java, estan los repos oficiales de Grpc: https://github.com/grpc/grpc-java.git 

* Un sampler es una entidad que genera un Request, puedes tener samplers REST, samplers SOAP, todo lo que va por HTTP puede ser emulado por Jmeter. Grpc va sobre HTTP, lo que necesitamos es editar un sampler que pueda hablar con nuestro contrato Grpc, para ello necesitas un cliente Grpc que conozca como hablar con el contrato y por esto necesitas compilar el contrato para agregarlo los archivos finales grpc-plugin.jar y grpc-gpon-lib.jar, este archivo es el que copiamos en el directorio de plugins (/usr/local/apache-jmeter-5.2/lib/ext/) que nos habilita para ejecutar las pruebas en Jmeter.

* Este plugin lo utilice con **Jmeter version 5.2.x**. Esta version de jmeter está versionado en este repositorio.
Fue tomado de este enlace (source):  wget https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.2.zip

* Usa maven este proyecto (https://maven.apache.org/download.cgi).También esta versionado en este repositorio.
No migre a usar gradle, solo use tal cual. pom.xml vendria a ser el equivalente a build.gradle. Usa las siguientes dependencias:

    * grpc-netty version: 1.19.0
    * grpc-netty-shaded version: 1.19.0
    * grpc-protobuf version: 1.19.0
    * grpc-stub version: 1.19.0 
    * protobuf-maven-plugin version: 0.5.0 (Para compilar el contrato y que la clase del sampler conozca como hablar con el endpoint Grpc)

* Use Java 8 para ejecutar Jmeter 5.2 y para compilar este proyecto **minimo Java 8**. Puede usar uno superior pero no es algo que solemos tener disponibles en los servidores del cliente.

* al momento de compilar el proto file, **es necesario** que no esté comprimido, por lo que se agrego al archivo del contrato (./generateJar/gpon/grpc-lib/src/main/proto/grpcextservice.proto) esta línea:
**Para producción esta línea no debería estar**, es de buena práctica que todas las clases se unifiquen en un sólo archivo.
````
option java_multiple_files = true;
````
# jmeter, gRPC, y Gpon Extension diagrama.

![jmeter, gRPC, y Gpon Extension](jmeterGpon.png)

# Diapositivas sobre jmeter y gpon (dhcp_logic)

* https://docs.google.com/presentation/d/1QcBodzlZnj5lke3obP3ezfc1uI14InpwEjzYW9qatLQ/edit?usp=sharing

# Diapositivas workshop

* https://docs.google.com/presentation/d/1sEqto0pSePIRVIqfkLD5jamvvDq0TkZBZb_t0jvWxf8/edit?usp=sharing

# Repositorios relacionados

* https://gitlab.intraway.com/jennifer.maldonado/devicefixtures [Generador de datos de dispositivos ONTs validos]

# Directorio donde se define el contrato (copiar y pegar aca el contrato). 

````
./jmeter-gpon/generateJar/gpon/grpc-lib/src/main/proto/ 

````

# Pasos para generar un nuevo Jar con una nueva definicion de contrato (actualizacion del contrato). 

Sustituye los paths de las variables: **JAVA_HOME** y  **PATH**  con lo que tienes en tu máquina local. 
Agregando el path de maven que se encuentra en **ESTE** repositorio.

````
export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_261;
export PATH=/media/disquito/code/jmeter-gpon/apache-maven-3.8.1/bin:$PATH;
cd ./generateJar/gpon/grpc-lib && mvn install;
````

### OUTPUT TERMINAL Building jar
````
[INFO] Building jar: /media/disquito/code/jmeter-gpon/generateJar/gpon/grpc-lib/target/grpc-gpon-lib-0.0.2.jar
[INFO] 
[INFO] --- maven-install-plugin:2.4:install (default-install) @ grpc-gpon-lib ---
[INFO] Installing /media/disquito/code/jmeter-gpon/generateJar/gpon/grpc-lib/target/grpc-gpon-lib-0.0.2.jar to /home/jennifer/.m2/repository/vn/intraway/grpc-gpon-lib/0.0.2/grpc-gpon-lib-0.0.2.jar
[INFO] Installing /media/disquito/code/jmeter-gpon/generateJar/gpon/grpc-lib/pom.xml to /home/jennifer/.m2/repository/vn/intraway/grpc-gpon-lib/0.0.2/grpc-gpon-lib-0.0.2.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.050 s
[INFO] Finished at: 2021-06-15T22:09:50-03:00
[INFO] ------------------------------------------------------------------------
````

Al finalizar el comando **mvn install**. Se genera un directorio **target** en ./generateJar/gpon/grpc-lib/ que contiene un JAR.
Este Jar file copiarlo en el directorio de extensiones y plugins de apache-jmeter-5.2
**Este Jar contiene la definición del contrato!!!!**

````
cd ./generateJar/gpon/grpc-lib; # optional only if your are in the repository ROOT path;
cp ./target/grpc-gpon-lib-0.0.2.jar   ../../../apache-jmeter-5.2/lib/ext;
ls -l ../../../apache-jmeter-5.2/lib/ext;
````

## Borrar este JAR 
````
cd ./generateJar/gpon/grpc-lib && mvn clean && cd ../../..;
````

# Pasos para generar un nuevo Jar del plugin Grpc Sampler (cuando quiero agregar mas lineas de debug o log).

````
export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_261;
export PATH=/media/disquito/code/jmeter-gpon/apache-maven-3.8.1/bin:$PATH;
mvn clean install;
````
Esto genera otra jar file necesario para hacer funcionar el plugin en jmeter version 5.2. y **editar el body del mensaje GRPC** es propiamente el Sampler!.

````
cp ./target/jmeter-grpc-client-sampler.jar ./apache-jmeter-5.2/lib/ext/;
ls -l ./apache-jmeter-5.2/lib/ext/;
````

### OUTPUT TERMINAL
````
[INFO] --- maven-install-plugin:2.4:install (default-install) @ jmeter-grpc-client-sampler ---
[INFO] Installing /media/disquito/code/jmeter-gpon/target/jmeter-grpc-client-sampler.jar to /home/jennifer/.m2/repository/vn/intraway/jmeter-grpc-client-sampler/0.1.2/jmeter-grpc-client-sampler-0.1.2.jar
[INFO] Installing /media/disquito/code/jmeter-gpon/dependency-reduced-pom.xml to /home/jennifer/.m2/repository/vn/intraway/jmeter-grpc-client-sampler/0.1.2/jmeter-grpc-client-sampler-0.1.2.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  7.964 s
[INFO] Finished at: 2021-06-15T22:07:07-03:00
[INFO] ------------------------------------------------------------------------
````